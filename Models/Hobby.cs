using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HobbyHub.Models
{
    [Table("hobby")]
    public class Hobby
    {
        [Key]
        public int HobbyId{get;set;}
        [Required]
        [MinLength(2, ErrorMessage="Must be at least 2 characters long")]
        public string Name{get;set;}
        [Required]
        [MinLength(10, ErrorMessage="Please provde detailed description; min 10 charcters.")]
        public string Description{get;set;}
        public DateTime CreatedAt {get;set;} = DateTime.Now;
        public DateTime UpdatedAt {get;set;} = DateTime.Now;
        public List<Enthusiast> Users{get;set;}
    }

    [Table("enthusiast")]
    public class Enthusiast
    {
        [Key]
        public int EnthusiastId {get;set;}
        public int UserId {get;set;}
        public int HobbyId {get;set;}
        public User User{get;set;}
        public Hobby Hobby{get;set;}

    }
}

