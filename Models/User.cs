using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HobbyHub.Models
{
    [Table("user")]
    public class User
    {
        [Key]
        public int UserId {get;set;}
        [Required]
        [MinLength(2, ErrorMessage = "Please use full first name")]
        [Display (Name = "First Name")]
        public string FirstName {get;set;}
        [Required]
        [MinLength(2, ErrorMessage = "Please use full last name")]
        [Display (Name = "Last Name")]
        public string LastName {get;set;}
        [Required]
        [Display (Name = "User Name")]
        [MinLength(3, ErrorMessage = "Username must be at least 3characters.")]
        [MaxLength(15, ErrorMessage = "Username must be less that 15 characters.")]
        public string UserName {get;set;}
        [Required]
        [DataType(DataType.Password)]
        [MinLength(8, ErrorMessage = "Password must be at least 8 characters")]
        public string Password {get;set;}
        [Required]
        [DataType(DataType.Password)]
        [Compare("Password")]
        [NotMapped]
        [Display (Name = "Confirm Password")]
        public string ConfirmPW {get;set;}
        public DateTime CreatedAt {get;set;} = DateTime.Now;
        public DateTime UpdatedAt {get;set;} = DateTime.Now;
        public List<Enthusiast> Hobbies{get;set;}

    }

    public class LogUser
    {
        [Required]
        [Display (Name = "User Name")]
        public string LogUsername {get;set;}
        [Required]
        [DataType(DataType.Password)]
        [Display (Name = "Password")]
        public string LogPassword {get;set;}
    } 
}