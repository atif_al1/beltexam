#pragma checksum "C:\Users\Atif\Documents\C#\HobbyHub\Views\Home\HobbyInfo.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "e7783fd70684f8dcb5a82668683e219db4ec62e6"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_HobbyInfo), @"mvc.1.0.view", @"/Views/Home/HobbyInfo.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/HobbyInfo.cshtml", typeof(AspNetCore.Views_Home_HobbyInfo))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\Atif\Documents\C#\HobbyHub\Views\_ViewImports.cshtml"
using HobbyHub;

#line default
#line hidden
#line 2 "C:\Users\Atif\Documents\C#\HobbyHub\Views\_ViewImports.cshtml"
using HobbyHub.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"e7783fd70684f8dcb5a82668683e219db4ec62e6", @"/Views/Home/HobbyInfo.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"73b43894b7f1b1132efb6bd8d82f780aceacb1c6", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_HobbyInfo : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Hobby>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(14, 6, true);
            WriteLiteral("\r\n<h1>");
            EndContext();
            BeginContext(21, 10, false);
#line 3 "C:\Users\Atif\Documents\C#\HobbyHub\Views\Home\HobbyInfo.cshtml"
Write(Model.Name);

#line default
#line hidden
            EndContext();
            BeginContext(31, 32, true);
            WriteLiteral("</h1>\r\n<h2>Description</h2>\r\n<p>");
            EndContext();
            BeginContext(64, 17, false);
#line 5 "C:\Users\Atif\Documents\C#\HobbyHub\Views\Home\HobbyInfo.cshtml"
Write(Model.Description);

#line default
#line hidden
            EndContext();
            BeginContext(81, 148, true);
            WriteLiteral("</p>\r\n\r\n<a href=\"/Hobby\"><u>Home</u></a>\r\n\r\n<table class =\"table table-bordered table-striped\">\r\n    <thead>\r\n        <tr>\r\n            <td><strong>");
            EndContext();
            BeginContext(230, 10, false);
#line 12 "C:\Users\Atif\Documents\C#\HobbyHub\Views\Home\HobbyInfo.cshtml"
                   Write(Model.Name);

#line default
#line hidden
            EndContext();
            BeginContext(240, 71, true);
            WriteLiteral(" Enthusiasts</strong></td>\r\n        </tr>\r\n    </thead>\r\n    <tbody> \r\n");
            EndContext();
#line 16 "C:\Users\Atif\Documents\C#\HobbyHub\Views\Home\HobbyInfo.cshtml"
         foreach(var user in ViewBag.Hobby.Users)
        {

#line default
#line hidden
            BeginContext(373, 38, true);
            WriteLiteral("            <tr>\r\n                <td>");
            EndContext();
            BeginContext(412, 19, false);
#line 19 "C:\Users\Atif\Documents\C#\HobbyHub\Views\Home\HobbyInfo.cshtml"
               Write(user.User.FirstName);

#line default
#line hidden
            EndContext();
            BeginContext(431, 1, true);
            WriteLiteral(" ");
            EndContext();
            BeginContext(433, 18, false);
#line 19 "C:\Users\Atif\Documents\C#\HobbyHub\Views\Home\HobbyInfo.cshtml"
                                    Write(user.User.LastName);

#line default
#line hidden
            EndContext();
            BeginContext(451, 26, true);
            WriteLiteral("</td>\r\n            </tr>\r\n");
            EndContext();
#line 21 "C:\Users\Atif\Documents\C#\HobbyHub\Views\Home\HobbyInfo.cshtml"
        }          

#line default
#line hidden
            BeginContext(498, 24, true);
            WriteLiteral("    </tbody>\r\n</table>\r\n");
            EndContext();
#line 24 "C:\Users\Atif\Documents\C#\HobbyHub\Views\Home\HobbyInfo.cshtml"
  
    if(ViewBag.Enthused == false)
    {

#line default
#line hidden
            BeginContext(568, 10, true);
            WriteLiteral("        <a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 578, "\"", 609, 2);
            WriteAttributeValue("", 585, "/addHobby/", 585, 10, true);
#line 27 "C:\Users\Atif\Documents\C#\HobbyHub\Views\Home\HobbyInfo.cshtml"
WriteAttributeValue("", 595, Model.HobbyId, 595, 14, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(610, 38, true);
            WriteLiteral("><button>Add to Hobbies</button></a>\r\n");
            EndContext();
#line 28 "C:\Users\Atif\Documents\C#\HobbyHub\Views\Home\HobbyInfo.cshtml"
    }
    else
    {

#line default
#line hidden
            BeginContext(672, 40, true);
            WriteLiteral("        <p>You are an Enthusiast!!</p>\r\n");
            EndContext();
#line 32 "C:\Users\Atif\Documents\C#\HobbyHub\Views\Home\HobbyInfo.cshtml"
    }



#line default
#line hidden
            BeginContext(726, 4, true);
            WriteLiteral("\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Hobby> Html { get; private set; }
    }
}
#pragma warning restore 1591
