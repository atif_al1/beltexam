#pragma checksum "C:\Users\Atif\Documents\C#\HobbyHub\Views\Home\Dashboard.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "4d707b6baf03de39b1ece9205de61672e51ab158"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Dashboard), @"mvc.1.0.view", @"/Views/Home/Dashboard.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/Dashboard.cshtml", typeof(AspNetCore.Views_Home_Dashboard))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\Atif\Documents\C#\HobbyHub\Views\_ViewImports.cshtml"
using HobbyHub;

#line default
#line hidden
#line 2 "C:\Users\Atif\Documents\C#\HobbyHub\Views\_ViewImports.cshtml"
using HobbyHub.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"4d707b6baf03de39b1ece9205de61672e51ab158", @"/Views/Home/Dashboard.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"73b43894b7f1b1132efb6bd8d82f780aceacb1c6", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Dashboard : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Hobby>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(14, 237, true);
            WriteLiteral("<h1>Hobbies</h1>\r\n<a href=\"logout\"><u>Logout</u></a>\r\n\r\n<table class=\"table table-bordered table-striped\">\r\n    <thead>\r\n        <tr>\r\n            <td>Name</td>\r\n            <td>Enthusiast</td>\r\n        </tr>\r\n    </thead>\r\n    <tbody>\r\n");
            EndContext();
#line 13 "C:\Users\Atif\Documents\C#\HobbyHub\Views\Home\Dashboard.cshtml"
         foreach(var hobby in ViewBag.Hobbies)
        {

#line default
#line hidden
            BeginContext(310, 40, true);
            WriteLiteral("            <tr>\r\n                <td><a");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 350, "\"", 377, 2);
            WriteAttributeValue("", 357, "Hobby/", 357, 6, true);
#line 16 "C:\Users\Atif\Documents\C#\HobbyHub\Views\Home\Dashboard.cshtml"
WriteAttributeValue("", 363, hobby.HobbyId, 363, 14, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(378, 4, true);
            WriteLiteral("><u>");
            EndContext();
            BeginContext(383, 10, false);
#line 16 "C:\Users\Atif\Documents\C#\HobbyHub\Views\Home\Dashboard.cshtml"
                                                 Write(hobby.Name);

#line default
#line hidden
            EndContext();
            BeginContext(393, 35, true);
            WriteLiteral("</u></a></td>\r\n                <td>");
            EndContext();
            BeginContext(429, 17, false);
#line 17 "C:\Users\Atif\Documents\C#\HobbyHub\Views\Home\Dashboard.cshtml"
               Write(hobby.Users.Count);

#line default
#line hidden
            EndContext();
            BeginContext(446, 26, true);
            WriteLiteral("</td>\r\n            </tr>\r\n");
            EndContext();
#line 19 "C:\Users\Atif\Documents\C#\HobbyHub\Views\Home\Dashboard.cshtml"
        }  

#line default
#line hidden
            BeginContext(485, 73, true);
            WriteLiteral("    </tbody>\r\n</table>\r\n<a href=\"/new\"><button>Add Hobby</button></u></a>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Hobby> Html { get; private set; }
    }
}
#pragma warning restore 1591
