﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using HobbyHub.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace HobbyHub.Controllers
{
    public class HomeController : Controller
    {
        private HobbyContext dbContext;
        public HomeController (HobbyContext context)
        {
            dbContext = context;
        }

        [HttpGet("")]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost("register")]
        public IActionResult Register(User newUser)
        {
            if(ModelState.IsValid)
            {
                var checkUsername = dbContext.Users.Any(u => u.UserName == newUser.UserName);
                if(checkUsername == true)
                {
                    ModelState.AddModelError("UserName","Username already in use.");
                    return View("Index");
                }
                else
                {
                    PasswordHasher<User> hasher = new PasswordHasher<User>();
                    newUser.Password = hasher.HashPassword(newUser, newUser.Password);
                    dbContext.Add(newUser);
                    dbContext.SaveChanges();
                    HttpContext.Session.SetInt32("user", newUser.UserId);
                    return RedirectToAction("Dashboard");
                }
            }
            return View("Index");
        }

        [HttpPost("login")]
        public IActionResult Login(LogUser newLogger)
        {
            if(ModelState.IsValid)
            {
                var user = dbContext.Users.FirstOrDefault(u => u.UserName == newLogger.LogUsername);
                if(user == null)
                {
                    TempData["Fail"] = "Username/Password not valid";                    return View("Index");
                }
                else
                {
                    var hasher = new PasswordHasher<LogUser>();
                    var result = hasher.VerifyHashedPassword(newLogger, user.Password, newLogger.LogPassword);
                    if(result == 0)
                    {
                        TempData["Fail"] = "Username/Password not valid";
                        return View("Index");
                    }
                    HttpContext.Session.SetInt32("user", user.UserId);
                    return RedirectToAction("Dashboard");
                }
            }
            return View("Index");
        }

        [HttpGet("Hobby")]
        public IActionResult Dashboard()
        {
            if(HttpContext.Session.GetInt32("user") != null)
            {
                ViewBag.Hobbies = dbContext.Hobbies
                    .Include(h => h.Users)
                    .ToList();
                return View();
            }
            return RedirectToAction("Index");
        }

        [HttpGet("Hobby/{hobbyId}")]
        public IActionResult HobbyInfo(int hobbyId)
        {
            if(HttpContext.Session.GetInt32("user") != null)
            {
                var hobby = dbContext.Hobbies.FirstOrDefault(h => h.HobbyId == hobbyId);
                if(hobby == null)
                {
                    return RedirectToAction("Dashboard");
                }
                var hobbyUser = dbContext.Hobbies
                    .Include(h => h.Users)
                    .ThenInclude(e => e.User)
                    .FirstOrDefault(h => h.HobbyId == hobbyId);
                
                ViewBag.Enthused = hobbyUser.Users.Any(u => u.UserId == HttpContext.Session.GetInt32("user"));
                ViewBag.Hobby = hobbyUser;

                return View(hobby);
            }
            return RedirectToAction("Dashboard");

        }

        [HttpGet("new")]
        public IActionResult NewHobby()
        {
            if(HttpContext.Session.GetInt32("user") != null)
            {
                return View();
            }
            return RedirectToAction("Index");
        }

        [HttpPost("verify")]
        public IActionResult Verify(Hobby newHobby)
        {
            if(HttpContext.Session.GetInt32("user") != null)
            {
                if(ModelState.IsValid)
                {
                    dbContext.Add(newHobby);
                    dbContext.SaveChanges();
                    return RedirectToAction("Dashboard");
                }
                return View("NewHobby");
            }
            return RedirectToAction("Index");
        }

        [HttpGet("addHobby/{hobbyId}")]
        public IActionResult addHobby(int hobbyId)
        {
            Enthusiast user = new Enthusiast();
            user.HobbyId  = hobbyId;
            user.UserId = (int)HttpContext.Session.GetInt32("user");
            dbContext.Add(user);
            dbContext.SaveChanges();
            return RedirectToAction("HobbyInfo");
        }

        [HttpGet("logout")]
        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index");
        }
    }
}
