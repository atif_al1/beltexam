﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HobbyHub.Migrations
{
    public partial class second : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "UpdateAt",
                table: "hobby",
                newName: "UpdatedAt");

            migrationBuilder.AlterColumn<string>(
                name: "UserName",
                table: "user",
                maxLength: 15,
                nullable: false,
                oldClrType: typeof(string));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "UpdatedAt",
                table: "hobby",
                newName: "UpdateAt");

            migrationBuilder.AlterColumn<string>(
                name: "UserName",
                table: "user",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 15);
        }
    }
}
